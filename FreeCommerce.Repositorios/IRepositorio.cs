﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeCommerce.Negocio.Dominio;
using FreeCommerce.Entidad;

namespace FreeCommerce.Repositorios
{
   public  interface IRepositorio<T> where T :  IEntidad
    {

        int Guardar(T entidad);
        int Guardar(IList<T> entidades);


        int Eliminar(T entidad);

        //T Obtener();


    }
}
