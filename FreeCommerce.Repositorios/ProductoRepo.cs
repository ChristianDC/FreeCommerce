﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using FreeCommerce.Negocio.Dominio;

namespace FreeCommerce.Repositorios
{
    public class ProductoRepo : IRepositorio<Producto>
    {


        SqlConnection _conexion;




        sealed class Procedures
        {

            public const string I_PRODUCTO = "I_PRODUCTO";

        }




        public int Eliminar(Producto entidad)
        {
            throw new NotImplementedException();
        }

        public int Guardar(IList<Producto> entidades)
        {
            return 0;

        }



        public int Guardar(Producto entidad)
        {
            using (_conexion = new SqlConnection("Data Source=Christian-PC;Initial Catalog=FreeCommerce;Integrated Security=true;"))
            {
                SqlCommand cmd = new SqlCommand(Procedures.I_PRODUCTO, _conexion);
                cmd.CommandType = System.Data.CommandType.StoredProcedure;
                cmd.Parameters.Add("id_prod", SqlDbType.UniqueIdentifier, 40).Value = entidad.Id;
                cmd.Parameters.Add("nom", SqlDbType.VarChar, 40).Value = entidad.Nombre;
                cmd.Parameters.Add("precio", SqlDbType.Decimal).Value = entidad.Precio;
                cmd.Parameters.Add("desc", SqlDbType.VarChar, 200).Value = entidad.Descripcion;

                _conexion.Open();
                int inserts = cmd.ExecuteNonQuery();
                _conexion.Close();

                return inserts;
            }
        }

        public List<Producto> ObtenerProductos()
        {
            using (_conexion = new SqlConnection("Data Source=Christian-PC;Initial Catalog=FreeCommerce;Integrated Security=true;"))
            {
                SqlCommand cmd = new SqlCommand("SELECT * FROM PRODUCTOS", _conexion);
                List<Producto> productos = new List<Producto>();

                _conexion.Open();
                using (SqlDataReader r = cmd.ExecuteReader())
                {
                    while (r.Read())
                    {
                        productos.Add(new Producto()
                        {
                            Id = Guid.Parse(r["Id_PRODUCTO"].ToString()),
                            Nombre = r["Nombre"].ToString(),
                            Precio = decimal.Parse(r["Precio"].ToString()),
                            Descripcion = r["Descripcion"].ToString()
                        });
                    }
                }
                _conexion.Close();
                return productos;
            }
        }

        //public Producto ObtenerProducto(Guid id)
        //{
        //    try
        //    {
        //         SqlCommand cmd = new SqlCommand("")
        //    }
        //    catch (Exception ex)
        //    {
        //        throw ex;
        //    }

        //}

        public bool ExisteProducto(Guid id)
        {
            try
            {
                SqlCommand cmd = new SqlCommand("select COUNT(*) from Productos WHERE Id = @id");

                cmd.Parameters.Add("id", SqlDbType.VarChar).Value = id;

                int scalar = (Int16)cmd.ExecuteScalar();

                return scalar > 0;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }



    }
}

