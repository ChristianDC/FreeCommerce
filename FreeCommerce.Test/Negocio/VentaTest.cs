﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using FreeCommerce.Negocio.Dominio;

namespace FreeCommerce.Test
{
    [TestClass]
    public class VentaTest
    {
        [TestMethod]
        public void Vender()
        {

            Usuario usuario = new Usuario()
            {
                Nombre = "Chris",
                Apellido = "Di Cos",
                Clave = "1234",
                Nick = "heavyMental",
                Email = "metal@gmail.com"
            };

            Venta venta = new Venta(usuario);

            Producto producto = CrearProductoTest();
            producto.Nombre = "Rockman 7";
            producto.Precio = 1800;

            VentaItem item = new VentaItem(producto, 1);
            venta.Items.Add(item);

            venta.Entrega = new DireccionEntrega()
            {
                Calle = "Pino",
                Numero = 1600,
                Ciudad = "Wilde",
                CodigoPostal = 1875
            };

            Assert.IsTrue(venta.Total > 0);
        }


        Producto CrearProductoTest()
        {
            Producto prod = new Producto()
            {
                Id = Guid.NewGuid(),
                //Nombre = "Rockman 7",
                //Precio = 1800
            };

            return prod;
        }

    }
}
