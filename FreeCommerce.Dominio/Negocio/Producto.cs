﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeCommerce.Entidad;

namespace FreeCommerce.Negocio.Dominio
{
    public class Producto : IEntidadNegocio
    {

        Guid _id;

        public Producto()
        {
            _id = Guid.NewGuid();
        }

        public Guid Id
        {
            get { return _id; }
            set { _id = value; }
        }

        public string Nombre
        {
            get; set;
        }

        public decimal Precio
        {
            get; set;
        }

        public string Descripcion
        {
            get; set;
        }

        public Validacion Validar()
        {
            Validacion val = new Validacion();

            if (Precio <= 0)
            {
                val.Validaciones.Add(new ValidacionItem("El precio no puede ser menor o igual a 0 ", ValidacionTipo.Advertencia));
            }
            
            if (string.IsNullOrEmpty(Nombre))
            {
                val.Validaciones.Add(new ValidacionItem("El nombre no puede estar vacio"));
            }
            
            return val;
        }

        public override string ToString()
        {
            return string.Format("{0} {1}", Id, Nombre);
        }

    }
}
