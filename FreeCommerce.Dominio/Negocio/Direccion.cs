﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeCommerce.Negocio.Dominio
{
    public class Direccion
    {

        public int Numero { get; set; }
        public string Calle { get; set; }
        public string Ciudad { get; set; }
        public int Piso { get; set; }
        public string Depto { get; set; }
        public int CodigoPostal { get; set; }

    }
}
