﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeCommerce.Negocio.Dominio
{
    public class Venta
    {

        List<VentaItem> _items = new List<VentaItem>();
        Usuario _usuario;
        DireccionEntrega _dirEntrega;

        public Venta(Usuario usuario)
        {
            _usuario = usuario;
        }

        public Usuario Usuario
        {
            get { return _usuario; }
            set { _usuario = value; }
        }

        public List<VentaItem> Items
        {
            get { return _items; }
        }

        public DireccionEntrega Entrega
        {
            get { return _dirEntrega; }
            set { _dirEntrega = value; }
        }

        public decimal Total
        {
            get
            {
                return Items.Sum(item => item.SubTotal);
            }
        }

    }
}
