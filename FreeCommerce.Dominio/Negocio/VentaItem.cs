﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeCommerce.Negocio.Dominio
{
    public class VentaItem
    {

        decimal _precioUnitario;
        decimal _subTotal;

        public VentaItem(Producto prod, Int16 cantidad)
        {
            Producto = prod;
            Cantidad = cantidad;
            _precioUnitario = prod.Precio;
        }

        public Int16 Cantidad { get; set; }
        public Producto Producto { get; set; }
        public decimal PrecioUnitario
        {
            get { return _precioUnitario; }
        }

        public decimal SubTotal
        {
            get { return _subTotal; }
            set { _subTotal = value; }
        }

    }
}
