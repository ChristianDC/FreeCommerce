﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeCommerce.Negocio
{

    public interface IPersona
    {


        string Nombre { get; }
    }

    public enum TipoDocumento
    {

    }

    public interface IPersonaFisica
    {
        string Nombre { get; set; }
        string Apellido { get; set; }

        //int NroDocumento { get; set; }
        //TipoDocumento TipoDocumento { get; set; }

    }

    public interface IPersonaJuridica : IPersona
    {

    }
}
