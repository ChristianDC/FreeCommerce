﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using FreeCommerce.Aplicacion;
using FreeCommerce.Aplicacion.DTO;
using FreeCommerce.Entidad;

namespace FreeCommerce.Administrador.Controllers
{
    public class HomeController : Controller
    {

        ProductoAplicacion _productoApp = new ProductoAplicacion();

        public ActionResult Index()
        {
            var vista = (ViewResult)View("Index");
            ViewBag.Title = "Nuevo producto";
            //ViewBag.Theme = "SbAdmin";
            return vista;
        }


        public ActionResult Prods()
        {
            List<ProductoDTO> prods = null;

            try
            {
                 prods = _productoApp.ObtenerProductos();
            }
            catch 
            {
                return new HttpStatusCodeResult(System.Net.HttpStatusCode.BadRequest);
                
            }

            return View("Productos", prods);
        }

        public ActionResult GuardarNuevo(ProductoDTO prod)
        {
            AplicacionResultado res = _productoApp.GuardarProducto(prod);

            if (!res.Validacion.EsValida)
                throw new NotImplementedException();

            return RedirectToAction("Prods");
        }


        public ActionResult Editar(ProductoDTO prod)
        {
            return View("Index");
        }


    }
}