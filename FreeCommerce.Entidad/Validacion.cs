﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeCommerce.Entidad
{

    public enum ValidacionTipo
    {
        Informacion, 
        Advertencia,
        Error
    }

   public class Validacion
    {

        public List<ValidacionItem> Validaciones  {get; set;  }  = new List<ValidacionItem>();

        public bool EsValida
        {
            get { return Validaciones.Count == 0; }
        }

    }
}
