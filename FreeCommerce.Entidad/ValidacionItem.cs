﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeCommerce.Entidad
{
    public class ValidacionItem
    {

        public ValidacionItem(string mensaje)
        {
            Tipo = ValidacionTipo.Error;
            Mensaje = mensaje;
        }

        public ValidacionItem(string mensaje, ValidacionTipo tipo) : this(mensaje)
        {
            Tipo = tipo;
        }


        public string Mensaje { get; set; }

        public ValidacionTipo Tipo { get; set; }

    }
}
