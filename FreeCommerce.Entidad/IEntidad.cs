﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FreeCommerce.Entidad
{
    public interface IEntidad
    {

        Guid Id { get; set; }

    }

    public interface IEntidadNegocio : IEntidad
    {
        Validacion Validar();
    }


}
