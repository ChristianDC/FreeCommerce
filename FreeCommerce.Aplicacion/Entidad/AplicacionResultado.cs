﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FreeCommerce.Entidad;

namespace FreeCommerce.Aplicacion
{
    public class AplicacionResultado
    {


        int _entidadesAfectadas;
        Validacion _validacion;

        public AplicacionResultado(Validacion validacion, int entidadesAfectadas)
        {
            _entidadesAfectadas = entidadesAfectadas;
            _validacion = validacion;
        }


        public Validacion Validacion
        {
            get { return _validacion; }
        }

        public int EntidadesAfectadas
        {
             get { return _entidadesAfectadas; }
        }


    }
}
